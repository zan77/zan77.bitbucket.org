/*global $, jQuery, Chart*/

var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/*


Koda za bolnisnice je v knjiznice/js/bolnisnice.js


*/




/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
 
 //Funkcija 3x kliče generirajPodatke
function generirajPodatkeDispatch(){
  for(var i=1;i<4;i++){
    
    generirajPodatke(i);
  }
  $("#generiraniEhrId").append("V padajočem meniju so avtomatsko posodobljeni EhrId za 3 paciente.</br>");
}
//Za stPacienta od 1 do 3 kliče funkcijo ustvariUporabnika s podatki posameznega pacienta
function generirajPodatke(stPacienta) {
  
  var times=["2010-04-21T13:02Z","2011-06-12T03:23Z","2012-12-03T07:54Z","2013-05-13T15:42Z","2014-07-18T23:13Z","2015-03-28T16:35Z"];
  
  switch(stPacienta){
    case 1:
      ustvariUporabnika("Dolla", "Bill", [142, 123, 119, 137, 145, 132],
      [93, 85, 73, 89, 95, 88], times, stPacienta);
      break;
    case 2:
      ustvariUporabnika("Bobby", "Axe", [100, 105, 122, 93, 82, 75],
      [74, 79, 85, 65, 58, 49], times, stPacienta);
      break;
    case 3:
      ustvariUporabnika("Chuck", "Rhoadie", [85, 113, 134, 152, 123, 115],
      [56, 77, 84, 96, 87, 66], times, stPacienta);
      break;
  }
  
}
//Povezava na EhrScape, ustvari nov ehrId, osebo in ji doda 6 meritvev
function ustvariUporabnika(ime, priimek, stlak, dtlak, time, u){
      $.ajaxSetup({
      headers: {
      "Authorization":getAuthorization()
      }
    });
    $.ajax({
      url:baseUrl+"/ehr",
      type:"POST",
      success: function(data){
        var ehrId=data.ehrId;
        var partyData={
          firstNames: ime,
          lastNames: priimek,
          partyAdditionalInfo:[{
            key:"ehrId",
            value:ehrId
          }]
        };
        //Izpis na index.html
        switch(u){
           case 1: u1ehr=ehrId;$("#generiraniEhrId").append("Novi EhrId za Dolla Bill: "+ehrId+"</br>");break;
           case 2: u2ehr=ehrId;$("#generiraniEhrId").append("Novi EhrId za Bobby Axe: "+ehrId+"</br>");break;
           case 3: u3ehr=ehrId;$("#generiraniEhrId").append("Novi EhrId za Chuck Rhoadie: "+ehrId+"</br>");break;
        }
        $.ajax({
          url:baseUrl+"/demographics/party",
          type:"POST",
          contentType: "application/json",
          data:JSON.stringify(partyData),
          success: function(party){
            for(var i=0;i<7;i++){
              var compositionData={
                "ctx/time": time[i],
                "ctx/language":"en",
                "ctx/territory":"SI",
                "vital_signs/blood_pressure/any_event/systolic":stlak[i],
                "vital_signs/blood_pressure/any_event/diastolic":dtlak[i],
              };
              var queryParams={
                ehrId:ehrId,
                templateId:"Vital Signs",
                format:"FLAT",
                
              };
              $.ajax({
                url:baseUrl+"/composition?"+$.param(queryParams),
                type:"POST",
                contentType:"application/json",
                data:JSON.stringify(compositionData),
                success:function(res){
                  //Nov uporabnik je narejen
                }
              });
            }
          }
        });
      }
    });
}

//EhrIdji za ponujene uporabnike iz dropdown menija (se spremenijo ob generiranju novih)
var u1ehr="858dea03-0f1b-415d-beeb-352c876bf45a";
var u2ehr="cac821fe-0e3f-4766-be52-98c626aa26f1";
var u3ehr="6ffcbd82-588e-4469-a519-d60f89a31e0e";
//Nadzorne spremenljivke za master-detail
var n1=false;
var n2=false;
var n3=false;


window.addEventListener("load", function(){
  //za graf
 var ctx=document.getElementById('chart').getContext('2d'); 
 
  document.getElementById("btnEhr").addEventListener("click", function(){
    var ehrId=document.getElementById("ehr").value;
    
    prikazi(ehrId, ctx);
  });
 //dropdown meni 
  document.getElementById("u1").addEventListener("click", function(){
    document.getElementById("ehr").value=u1ehr;
  });
  document.getElementById("u2").addEventListener("click", function(){
    document.getElementById("ehr").value=u2ehr;
  });
  document.getElementById("u3").addEventListener("click", function(){
    document.getElementById("ehr").value=u3ehr;
  });
  //master-detail
  document.getElementById("nasvet1").addEventListener("click", function(){
    if(n1){
      document.getElementById("n1").innerHTML="";
      n1=false;
    }
    else {
      document.getElementById("n1").innerHTML="<li><ul><strong>Manj soli</strong></ul>\
      <ul><strong>Več sadja in zelenjave</strong></ul></li>";
      n1=true;
    }
  });
  document.getElementById("nasvet2").addEventListener("click", function(){
    if(n2){
      document.getElementById("n2").innerHTML="";
      n2=false;
    }
    else {
      document.getElementById("n2").innerHTML="<strong>Manj alkohola</strong>";
      n2=true;
    }
  });
  document.getElementById("nasvet3").addEventListener("click", function(){
    if(n3){
      document.getElementById("n3").innerHTML="";
      n3=false;
    }
    else {
      document.getElementById("n3").innerHTML="Priporočena je vsaj 30 minutna fizična aktivnost 5x na teden.";
      n3=true;
    }
  });

 
  
});
//se izvede ob kliku na gumb prikaži
function prikazi(ehrId, ctx){
    //dobi podatke o krvnem tlaku iz EhrScape
    $.ajax({
    url:baseUrl+"/view/"+ehrId+"/blood_pressure",
    type:"GET",
    headers:{
      "Authorization":getAuthorization()
    },
    success: function(res){
      //dobi podatke o mediani iz WHO
      $.ajax({
		  url: "https://apps.who.int/gho/athena/api/GHO/BP_06.json?filter=COUNTRY:SVN;SEX:MLE", 
		  dataType: 'jsonp',
		  success: function (resWHO) {
		    //uredi podatke iz WHO
		    var letaWHO=[];
		    var vrednostiWHO=[];
		    for(var el in resWHO.fact){
		      
  		    for(var i in resWHO.fact[el].Dim){
  		      if(resWHO.fact[el].Dim[i].category=="YEAR"){
  		        for(var j in res){
  		          var parse=res[j].time.split("-")[0];
  		          if(parse==resWHO.fact[el].Dim[i].code){
  		            letaWHO[j]=parse;
  		            vrednostiWHO[j]=resWHO.fact[el].value.numeric;
  		          }
  		        }
  		      }
  		    }
  		    
  		   
  		  }

  		  var WHOdata=vrednostiWHO.slice(0,letaWHO.length);
  		  
  		  //nariše graf
  		  vizualiziraj(res, ctx, WHOdata);
		  }
      });
    }
  });
 
  
}
//se kliče po funkciji prikazi(), nariše graf
var myChart;
function vizualiziraj(res, ctx, WHOdata){
  if(myChart!=undefined){myChart.destroy();}
  var xos=[];
  var yoss=[];
  var yosd=[];
  for(var i=0;i<res.length;i++){
    xos[i]=res[i].time;
    yoss[i]=res[i].systolic;
    yosd[i]=res[i].diastolic;
  }
   myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: xos,
        datasets: [{
            label: 'Vaš sistolični krvni tlak',
            data: yoss,
            borderColor: "#FFDC00",
            fill: false

        }, 
        {
          label:'Vaš diastolični krvni tlak',
          data:yosd,
          borderColor: "#3377FF",
          fill:false
        },
        {
          label: 'Mediana sistoličnega krvnega tlaka za slovenske moške (podatki do leta 2015)',
          data: WHOdata,
          borderColor: "#2e613b",
          fill: false
        }
        ]
    },
    options: {
      elements:{
        line:{
          tension:0
        }
      },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: false
                },
                scaleLabel: {
                  display: true,
                  labelString:"mmHg"
                }
            }]
        }
    }
});

}



   

    
    
    
    
    
  

