// Zamljevid bolnisnic
/*global L, $, distance*/
//shranjene oznake po pridobitvi iz json datoteke
var oznake=[];
//podatki o oznakah (wheelchair, ime)
var oznake2=[];
//shranjene Point oznake
var oznakeP=[];
var oznakeP2=[];
//radij obarvanja 2 km
var MAX_DIST=2;
var mymap;
window.addEventListener('load', function () {
  //zacetni prikaz bolnisnic
  mymap=L.map("mapid").setView([46.0536077, 14.5247825], 14);
  L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(mymap);
   $.getJSON("./knjiznice/json/bolnisnice.json", function(json){
    prikaziBolnisnice(json);
    //click listener
    mymap.on("click", pobarvajBliznje)
   });
 
});
//za point markerje
var ikonaB = new L.Icon({
    iconUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/marker-icon-2x-blue.png',
    shadowUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' + 
      'marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
  });
var ikonaG = new L.Icon({
  iconUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/marker-icon-2x-green.png',
  shadowUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' + 
    'marker-shadow.png',
  iconSize: [25, 41],
  iconAnchor: [12, 41],
  popupAnchor: [1, -34],
  shadowSize: [41, 41]
});
//uredi podatke iz bolnisnice.json in jih shrani v array
function prikaziBolnisnice(json){
  var barva="blue";
  for(var i=0; i<json.features.length;i++){
    var zac=json.features[i].geometry.coordinates;
    var temp;
    //Check if object is point
    if(json.features[i].geometry.type=="Point"){
      temp=zac[0];
      zac[0]=zac[1];
      zac[1]=temp;
      var oznaka=L.marker(zac, {icon:ikonaB}).addTo(mymap);
      oznakeP.push(oznaka);
      oznakeP2.push({name:json.features[i].properties.name, wheelchair:json.features[i].properties.wheelchair});
      
    }
    //Check if object is LineString
    else if(json.features[i].geometry.type=="LineString"){
      for(var j=0;j<zac.length;j++){
        temp=zac[j][0];
        zac[j][0]=zac[j][1];
        zac[j][1]=temp;
      }
      var oznaka=L.polygon(zac).addTo(mymap);
      oznake.push(oznaka);
      oznake2.push({name:json.features[i].properties.name, wheelchair:json.features[i].properties.wheelchair});
    }
    //Check if object is Polygon
    else if(json.features[i].geometry.type=="Polygon"){
      for(var j=0;j<zac.length;j++){
        for(var k=0;k<zac[j].length;k++){
          temp= zac[j][k][0];
          zac[j][k][0]=zac[j][k][1];
          zac[j][k][1]=temp;
        }
      }
      var oznaka= L.polygon(zac, {color:barva}).addTo(mymap);
      oznake.push(oznaka);
      oznake2.push({name:json.features[i].properties.name, wheelchair:json.features[i].properties.wheelchair});
    }
   
  }
  
}
var clickPos=L.marker([0,0]);
//ob kliku na zemljevid gre skozi tabele in preveri če so za pobarvati
function pobarvajBliznje(e){
  clickPos.removeFrom(mymap);
  clickPos=L.marker(e.latlng).addTo(mymap);
  for(var i=0; i<oznake.length;i++){
      var wheelchair= oznake2[i].wheelchair==undefined ? "unknown" : oznake2[i].wheelchair;
      var name= oznake2[i].name==undefined ? "Unkonwn name" : oznake2[i].name;
      if(distance(e.latlng.lat, e.latlng.lng, oznake[i]._latlngs[0][0].lat, oznake[i]._latlngs[0][0].lng,"K")<MAX_DIST){
        oznake[i].setStyle({
          color:"green"
        });
      }
      else{
        oznake[i].setStyle({
          color:"blue"
        });      
      }
      oznake[i].bindPopup(name+"</br>Wheelchair accesible: "+wheelchair);
    }
  for(var i=0;i<oznakeP.length;i++){
    var wheelchair= oznakeP2[i].wheelchair==undefined ? "unknown" : oznakeP2[i].wheelchair;
    var name= oznake2[i].name==undefined ? "Unkonwn name" : oznake2[i].name;
      if(distance(e.latlng.lat, e.latlng.lng, oznakeP[i]._latlng.lat, oznakeP[i]._latlng.lng,"K")<MAX_DIST){
        var latlngs=oznakeP[i].getLatLng();
        oznakeP[i].removeFrom(mymap);
        var m=L.marker(latlngs, {icon:ikonaG}).addTo(mymap);
        m.bindPopup(name+"</br>Wheelchair accesible: "+wheelchair);
      }
      else{
        var latlngs=oznakeP[i].getLatLng();
        oznakeP[i].removeFrom(mymap);
        var m=L.marker(latlngs, {icon:ikonaB}).addTo(mymap);
        m.bindPopup(name+"</br>Wheelchair accesible: "+wheelchair);
      }
  }
    
    
  
  
  
}